#Utilisation

Pour pouvoir lancer le logiciel, il est nécessaire de d'abord crée une base de donnée.
Une fois que la BDD est créée, rendez-vous dans la classe __Application__ et effectuez les
changements nécessaires.

#Lancement

Une fois que vous avez fini votre configuration, lancer le logiciel depuis votre IDE avec le bouton Play.
Vous aurez les logs dans la console et le logiciel se lancera après que tout soit prêt.

#A propos du MCD

Dans le dossier "resources", le fichier .mcd s'y trouve, si jamais la base de donnée n'arrive pas
à charger les requêtes.
###POINT IMPORTANT
Lors de l'héritage "eleves" et "professeurs", ils doivent contenir seulement l'**id_personne** !
