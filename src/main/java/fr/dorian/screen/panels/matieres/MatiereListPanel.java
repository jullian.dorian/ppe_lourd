package fr.dorian.screen.panels.matieres;

import fr.dorian.Application;
import fr.dorian.content.Matiere;
import fr.dorian.database.Database;
import fr.dorian.screen.fields.table.MatiereTable;

import javax.swing.*;
import javax.swing.plaf.basic.BasicPanelUI;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * Class créée le 12/03/2019 à 12:19
 * par Jullian Dorian
 */
public class MatiereListPanel extends JPanel{

    public MatiereListPanel(JFrame frame) {
        this.setUI(new BasicPanelUI());
        this.setLayout(new BorderLayout());

        Database database = Application.getDatabase();

        MatiereTable table = new MatiereTable(Application.toTab(Application.getMatiereList().values()));
        table.setAutoCreateRowSorter(true);

        TableRowSorter<TableModel> sorter = new TableRowSorter<TableModel>(table.getModel());
        sorter.setSortable(1, true);
        sorter.setSortsOnUpdates(true);
        table.setRowSorter(sorter);

        JScrollPane jScrollPane = new JScrollPane(table);
        add(jScrollPane, BorderLayout.NORTH);

        JPanel panel = new JPanel();
        panel.setLayout(new FlowLayout());

        JButton delete = new JButton("Supprimer (Non dispo)");
        delete.addActionListener(e -> {
            //Faut gérer les liaisons avec les professeurs et faire des id non null etc...
            Matiere selected = table.getSelected();
            /*
            if(selected != null) {

                boolean confirm = JOptionPane.showConfirmDialog(this, "Êtes vous sûr de vouloir supprimer la classe ? Toutes les séances ayant la classe seront supprimés.", "Confirmer", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION;

                if(confirm) {
                    database.connectDb();

                    try {
                        PreparedStatement statement = database.getConnection().prepareStatement(
                                "DELETE FROM matieres WHERE id_matiere=" + selected.getId());

                        if(statement.executeUpdate() > 0) {
                            Application.getProfesseurList().forEach((k, v) -> {
                                if(v != null && v.getMatiere().getId() == selected.getId()) {
                                    v.setMatiere(new Matiere(-1, "Aucune matière"));
                                }
                            });
                            Application.getMatiereList().remove(selected.getId());
                            table.removeFrom(selected.getId());
                            table.updateUI();

                            JOptionPane.showMessageDialog(this, "La classe a bien été supprimé.");
                        } else {
                            JOptionPane.showMessageDialog(this, "Impossible de supprimer la classe.");
                        }

                    } catch (SQLException e1) {
                        e1.printStackTrace();
                    } finally {
                        database.disconnectDb();
                    }
                } else {
                    JOptionPane.showMessageDialog(this, "La classe n'a pas été supprimé.");
                }
            }

             */
        });
        panel.add(delete);

        JTextField field = new JTextField();
        field.setPreferredSize(new Dimension(100, 20));
        field.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {}

            @Override
            public void keyPressed(KeyEvent e) {}

            @Override
            public void keyReleased(KeyEvent e) {
                sorter.setRowFilter(RowFilter.regexFilter(field.getText()));
            }
        });

        panel.add(new JLabel("Filtrer : "));
        panel.add(field);
        add(panel, BorderLayout.SOUTH);

        frame.pack();
    }

}
