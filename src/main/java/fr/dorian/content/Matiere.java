package fr.dorian.content;

import fr.dorian.Application;
import fr.dorian.database.Database;

import java.awt.*;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Class créée le 11/03/2019 à 16:13
 * par Jullian Dorian
 */
public class Matiere implements ITable{

    private final Database database = Application.getDatabase();

    private int id_matiere;
    private final String label;
    private final Color color;

    public Matiere(int id_matiere, String label) {
        this.id_matiere = id_matiere;
        this.label = label;
        ThreadLocalRandom rand = ThreadLocalRandom.current();
        this.color = new Color(rand.nextInt(150, 256), rand.nextInt(150, 256), rand.nextInt(150, 256));
    }

    public int getId() {
        return id_matiere;
    }

    @Override
    public boolean register() {
        final Map<String, Object> options = new HashMap<>();
        options.put("label", label);

        this.id_matiere = database.insert("matieres", options, Statement.RETURN_GENERATED_KEYS);
        return getId() > 0;
    }

    public String getLabel() {
        return label;
    }

    public Color getColor() {
        return color;
    }

    @Override
    public String toString() {
        return label;
    }
}
