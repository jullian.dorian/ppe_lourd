package fr.dorian.task;

import fr.dorian.content.*;
import fr.dorian.database.Database;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

/**
 * Class créée le 15/03/2019 à 09:26
 * par Jullian Dorian
 */
public class LoadTask implements Runnable {

    private final Database database;

    private Map<Integer, Eleve> eleveList = new HashMap<>();
    private Map<Integer, Professeur> professeurList = new HashMap<>();
    private Map<Integer, Parent> parentList = new HashMap<>();

    private Map<Integer, Matiere> matiereList = new HashMap<>();
    private Map<Integer, Classe> classeList = new HashMap<>();
    private Map<Integer, Salle> salleList = new HashMap<>();
    private Map<Integer, Seance> seanceList = new HashMap<>();

    public LoadTask(Database database) {
        this.database = database;
    }

    /**
     * When an object implementing interface <code>Runnable</code> is used
     * to create a thread, starting the thread causes the object's
     * <code>run</code> method to be called in that separately executing
     * thread.
     * <p>
     * The general contract of the method <code>run</code> is that it may
     * take any action whatsoever.
     *
     * @see Thread#run()
     */
    @Override
    public void run() {
        //connection
        database.connectDb();

        if(!database.isConnected()) {
            throw new RuntimeException("Can't connect to the database, please verify login infos.");
        }

        Connection connection = database.getConnection();

        try {
            createDefaultTables(connection);

            //Chargement des eleves
            final PreparedStatement statementEleve = connection.prepareStatement("SELECT e.*, groupe FROM eleves INNER JOIN personne e on eleves.id_personne = e.id_personne");
            ResultSet resultSet = statementEleve.executeQuery();

            while(resultSet.next()) {
                Eleve eleve = new Eleve(
                        resultSet.getInt(1),
                        resultSet.getString(2),
                        resultSet.getString(3),
                        resultSet.getDate(4),
                        resultSet.getString(5),
                        resultSet.getString(6)
                );
                eleve.setGroupe(resultSet.getString(7));
                this.eleveList.put(eleve.getId(), eleve);
            }
            statementEleve.close();
            /* ---- */

            final PreparedStatement statementMat = connection.prepareStatement("SELECT * FROM matieres");
            resultSet = statementMat.executeQuery();

            while(resultSet.next()) {
                Matiere matiere = new Matiere(
                        resultSet.getInt(1),
                        resultSet.getString(2)
                );

                this.matiereList.put(matiere.getId(), matiere);
            }

            statementMat.close();
            /* ---- */

            final PreparedStatement statementProfessor = connection.prepareStatement("SELECT e.*, id_matiere FROM professeurs INNER JOIN personne e on professeurs.id_personne = e.id_personne");
            resultSet = statementProfessor.executeQuery();

            while(resultSet.next()) {
                Professeur professeur = new Professeur(
                        resultSet.getInt(1),
                        resultSet.getString(2),
                        resultSet.getString(3),
                        resultSet.getDate(4),
                        resultSet.getString(5),
                        resultSet.getString(6),
                        matiereList.get(resultSet.getInt(7)) //On récupère la matière via l'id-> key
                );

                this.professeurList.put(professeur.getId(), professeur);
            }

            statementProfessor.close();
            /* ---- */
            final PreparedStatement statementParent = connection.prepareStatement("SELECT * FROM personne WHERE id_personne NOT IN (SELECT e.id_personne FROM eleves e) AND" +
                    " id_personne NOT IN (SELECT p.id_personne FROM professeurs p) ");
            resultSet = statementParent.executeQuery();

            while(resultSet.next()) {
                Parent parent = new Parent(
                        resultSet.getInt(1),
                        resultSet.getString(2),
                        resultSet.getString(3),
                        resultSet.getDate(4),
                        resultSet.getString(5),
                        resultSet.getString(6)
                );

                this.parentList.put(parent.getId(), parent);
            }

            statementParent.close();
            /* ---- */
            final PreparedStatement statementSalle = connection.prepareStatement("SELECT * FROM salles");
            resultSet = statementSalle.executeQuery();

            while(resultSet.next()) {
                Salle salle = new Salle(
                        resultSet.getInt(1),
                        resultSet.getInt(2),
                        resultSet.getInt(3),
                        resultSet.getInt(4)
                );

                this.salleList.put(salle.getId(), salle);
            }

            statementSalle.close();
            /* ---- */
            final PreparedStatement statementClasse = connection.prepareStatement("SELECT * FROM classes");
            resultSet = statementClasse.executeQuery();

            while(resultSet.next()) {
                Classe classe = new Classe(
                        resultSet.getInt(1),
                        resultSet.getString(2)
                );

                this.classeList.put(classe.getId(), classe);
            }

            statementClasse.close();
            /* ---- MISE EN PLACE DE LA RELATION parent<->eleve ---- */
            final PreparedStatement statementRelPE = connection.prepareStatement("SELECT * FROM avoir_parent");
            resultSet = statementRelPE.executeQuery();

            while(resultSet.next()) {

                Eleve eleve = eleveList.get(resultSet.getInt("id_eleve"));
                Parent parent = parentList.get(resultSet.getInt("id_parent"));

                eleve.addParent(parent);
                parent.addEnfant(eleve);
            }

            statementRelPE.close();
            /* ---- MISE EN PLACE DE LA RELATION classe<->eleve ---- */
            final PreparedStatement statementRelEC = connection.prepareStatement("SELECT * FROM etre_dans");
            resultSet = statementRelEC.executeQuery();

            while(resultSet.next()) {

                //String groupe = resultSet.getString("groupe");
                Eleve eleve = eleveList.get(resultSet.getInt("id_personne"));
                Classe classe = classeList.get(resultSet.getInt("id_classe"));

                eleve.setClasse(classe);
                classe.addEleve(eleve);
            }

            statementRelEC.close();
            /* ---- SEANCE ---- */
            final PreparedStatement statementSeance = connection.prepareStatement("SELECT * FROM seance ORDER BY date_debut ASC");
            resultSet = statementSeance.executeQuery();

            while(resultSet.next()) {

                Date sqlDateSta = new Date(resultSet.getLong("date_debut"));
                Date sqlDateEnd = new Date(resultSet.getLong("date_fin"));

                Calendar calendar = Calendar.getInstance();
                calendar.setTimeInMillis(sqlDateSta.getTime());
                Calendar calendar1 = Calendar.getInstance();
                calendar1.setTimeInMillis(sqlDateEnd.getTime());

                Seance seance = new Seance(
                        calendar,
                        calendar1,
                        professeurList.get(resultSet.getInt("id_personne")),
                        classeList.get(resultSet.getInt("id_classe")),
                        salleList.get(resultSet.getInt("id_salle"))
                );

                seanceList.put(seanceList.size()+1, seance);
            }

            statementSeance.close();

            System.out.println("Aucune erreur n'a été trouvé lors de la récupération des données.");

        } catch (SQLException e) {
            e.printStackTrace();
        }

        //deconnexion
        database.disconnectDb();
    }

    /**
     * Est effectué pour savoir si les tables doivent être crée si elles ne le sont pas.
     * @param connection
     */
    private void createDefaultTables(Connection connection) throws SQLException {

        //Matieres
        PreparedStatement createTable = connection.prepareStatement("CREATE TABLE IF NOT EXISTS matieres (id_matiere INT NOT NULL AUTO_INCREMENT, label varchar(100), PRIMARY KEY (id_matiere)) ENGINE=InnoDB;");
        int created = createTable.executeUpdate();

        //Personne
        createTable = connection.prepareStatement("CREATE TABLE IF NOT EXISTS personne(" +
                "id_personne Int NOT NULL AUTO_INCREMENT," +
                "nom Varchar (50) NOT NULL," +
                "prenom Varchar (50) NOT NULL," +
                "date_naissance Date NOT NULL," +
                "telephone Varchar (10)," +
                "email Varchar (255)," +
                "CONSTRAINT personne_PK PRIMARY KEY (id_personne)" +
                ")ENGINE=InnoDB;");
        created = createTable.executeUpdate();

        //Eleves
        createTable = connection.prepareStatement("CREATE TABLE IF NOT EXISTS eleves(" +
                "id_personne Int NOT NULL ," +
                "groupe Varchar (2) DEFAULT 'A'," +
                "CONSTRAINT eleves_PK PRIMARY KEY (id_personne)" +
                ",CONSTRAINT eleves_personne_FK FOREIGN KEY (id_personne) REFERENCES personne(id_personne)" +
                ")ENGINE=InnoDB;");
        created = createTable.executeUpdate();

        //Professeur
        createTable = connection.prepareStatement("CREATE TABLE IF NOT EXISTS professeurs(" +
                "id_personne Int NOT NULL," +
                "id_matiere Int" +
                ",CONSTRAINT professeurs_PK PRIMARY KEY (id_personne)" +
                ",CONSTRAINT professeurs_personne_FK FOREIGN KEY (id_personne) REFERENCES personne(id_personne)" +
                ",CONSTRAINT professeurs_matieres0_FK FOREIGN KEY (id_matiere) REFERENCES matieres(id_matiere)" +
                ")ENGINE=InnoDB;");
        created = createTable.executeUpdate();

        //Classes
        createTable = connection.prepareStatement("CREATE TABLE IF NOT EXISTS classes(" +
                "id_classe Int AUTO_INCREMENT NOT NULL ," +
                "section Varchar (50) NOT NULL" +
                ",CONSTRAINT classes_PK PRIMARY KEY (id_classe)" +
                ")ENGINE=InnoDB;");
        created = createTable.executeUpdate();

        //Salles
        createTable = connection.prepareStatement("CREATE TABLE IF NOT EXISTS salles(" +
                "id_salle Int  AUTO_INCREMENT  NOT NULL ," +
                "n_salle Int NOT NULL," +
                "n_etage Int NOT NULL ," +
                "nb_place Int NOT NULL" +
                ",CONSTRAINT salles_PK PRIMARY KEY (id_salle)" +
                ")ENGINE=InnoDB;");
        created = createTable.executeUpdate();

        //Seances
        createTable = connection.prepareStatement("CREATE TABLE IF NOT EXISTS seance(" +
                "id_seance Int AUTO_INCREMENT NOT NULL," +
                "date_debut Date NOT NULL," +
                "date_fin Date NOT NULL," +
                "id_personne Int NOT NULL," +
                "id_salle Int NOT NULL," +
                "id_classe Int NOT NULL" +
                ",CONSTRAINT seance_PK PRIMARY KEY (id_seance)" +
                ",CONSTRAINT seance_professeurs_FK FOREIGN KEY (id_personne) REFERENCES professeurs(id_personne)" +
                ",CONSTRAINT seance_salles0_FK FOREIGN KEY (id_salle) REFERENCES salles(id_salle)" +
                ",CONSTRAINT seance_classes1_FK FOREIGN KEY (id_classe) REFERENCES classes(id_classe)" +
                ")ENGINE=InnoDB;");
        created = createTable.executeUpdate();

        //relation classes eleves
        createTable = connection.prepareStatement("CREATE TABLE IF NOT EXISTS etre_dans(" +
                "id_classe Int NOT NULL ," +
                "id_personne Int NOT NULL" +
                ",CONSTRAINT etre_dans_PK PRIMARY KEY (id_classe,id_personne)" +
                ",CONSTRAINT etre_dans_classes_FK FOREIGN KEY (id_classe) REFERENCES classes(id_classe)" +
                ",CONSTRAINT etre_dans_eleves0_FK FOREIGN KEY (id_personne) REFERENCES eleves(id_personne)" +
                ")ENGINE=InnoDB;");
        created = createTable.executeUpdate();

        //relation parents eleves
        createTable = connection.prepareStatement("CREATE TABLE IF NOT EXISTS avoir_parent(" +
                "id_personne Int NOT NULL ," +
                "id_personne_avoir_parent Int NOT NULL" +
                ",CONSTRAINT avoir_parent_PK PRIMARY KEY (id_personne,id_personne_avoir_parent)" +
                ",CONSTRAINT avoir_parent_eleves_FK FOREIGN KEY (id_personne) REFERENCES eleves(id_personne)" +
                ",CONSTRAINT avoir_parent_personne0_FK FOREIGN KEY (id_personne_avoir_parent) REFERENCES personne(id_personne)" +
                ")ENGINE=InnoDB;");
        created = createTable.executeUpdate();
    }


    public Map<Integer, Eleve> getEleveList() {
        return eleveList;
    }

    public Map<Integer, Professeur> getProfesseurList() {
        return professeurList;
    }

    public Map<Integer, Parent> getParentList() {
        return parentList;
    }

    public Map<Integer, Classe> getClasseList() {
        return classeList;
    }

    public Map<Integer, Salle> getSalleList() {
        return salleList;
    }

    public Map<Integer, Seance> getSeanceList() {
        return seanceList;
    }

    public Map<Integer, Matiere> getMatiereList() {
        return matiereList;
    }
}
